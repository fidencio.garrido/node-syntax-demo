const express = require("express"),
	app = express(),
	log = require("winston"),
	Configuration = require("./config"),
	config = new Configuration(),
	PORT = config.get("PORT", 3020),
	lib = require("./libdemo");

app.get("/", (req, res)=>{
	res.send("hello world");	
});

app.get("/slowcall", async (req, res)=>{
	await lib.doSomethingSlow();
	res.send("cool!");
});

app.get("/another", lib.externalHandler);
	
app.listen( PORT,  ()=>{
	log.info(`app_port=${PORT}`);	
});