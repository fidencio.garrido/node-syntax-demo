class Config {
	get(configName, defaultValue=null){
		return process.env[configName] || defaultValue;
	}
	
	constructor(){
		this.configMap = {};
	}
}

module.exports = Config;