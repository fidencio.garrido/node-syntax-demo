/**
 * Demo of object
 */
const Lib = {
	simpleLambda(res){
		const localArray = [1,2,3,4,5,6,7];
		const filtered = localArray.filter( item=> item<5);
		res.send(filtered);
	},
	doSomethingSlow(){
		return new Promise( (resolve, reject)=>{
			setTimeout(resolve, 1000);
		});
	},
	externalHandler(req, res){
		Lib.simpleLambda(res);
	}
};

module.exports = Lib;